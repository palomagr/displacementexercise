# The Displacement Exercise (DEX)

DEX is a small materials testing machine that should be capable of running tensile and compressive tests at up to 600N of force.

| | |
| --- | --- |
| ![dex](images/2019-10-17_dex-01.jpg) | ![dex](images/2019-10-17_dex-03.jpg) |

## Operating Principle

A [stress - strain plot](https://en.wikipedia.org/wiki/Stress%E2%80%93strain_curve) is a very useful piece of information when characterizing materials.

![ss curve](images/stress-strain.jpg)

To generate these curves, the DEX slowly pulls samples (normally some 'dogbone' shape [below](#testing-notes)) apart, while measuring the amount that it stretches (~ the strain), and the amount of force it exerts as it is stretched (~ the stress). These types of machine are common in industry, commonly referred to by their leading brand name 'Instron', or as 'Universal Testing Machines'.

## Hardware

The DEX is an open-source piece of materials testing equipment. The machine can be manufactured by anyone with access to a laser cutter with at least a 24x12" bed, and nearly any FDM 3D Printer. A bill of materials of purchased parts required to complete the machine is below, totaling some ~ $500 USD.  

The machine is made largely from [laser cut](https://hackaday.com/2015/09/03/how-to-build-anything-using-delrin-and-a-laser-cutter/) [delrin](https://hackaday.com/2015/09/22/drawbacks-of-lased-delrin-and-how-to-slip-around-them/) and 3D printed (commodity FDM) parts.

More detailed documentation will follow.

### CAD

CAD for the machine is available in this Repo, [under `cad/fusion`](https://gitlab.cba.mit.edu/jakeread/displacementexercise/tree/master/cad/fusion) - the `.f3z` file is a Fusion 360 parametric model of the machine. To source parts, consult the BOM below.

![dex](images/2019-09-27_DEX-CAD.png)

### BOM

Part numbers are from [McMaster Carr](http://mcmaster.com) unless otherwise linked.

| Part | Spec | Count | Notes | PN / Link | Cost / Per |
| --- | --- | ---: | --- | ---: | ---: |
| Delrin (Acetal) Cast Sheet | 24x12", 0.25" Thick | 2 | Fits on 24x18" w/ no mistakes | 8573K35 | $50.49 / 1 |
| PLA 'Tough' |  | ~ 300g | Many 3DP Mechanical Bits | [Matter Hackers M6E9T65K](https://www.matterhackers.com/store/l/light-blue-pro-series-tough-pla-filament-175mm-1kg/sk/M6E9T65K) | $52.00 / 1kg |
| 625ZZ Bearings | 5x16x5  | 24 | - | [VXB 625ZZ](https://www.vxb.com/20-625ZZ-Shielded-5mm-Bore-Diameter-Miniature-p/625zz20.htm) | $24.95 / 20 |
| Bearing Shims | 5x10x0.5 | 37 | - | 98055A098 | $8.62 / 50 |
| Carriage Shoulders | M4x5x6 | 12 | - | 92981A146 | $2.16 / 1 |
| Reduction Shoulder | M4x5x10 | 1 | - | 92981A030 | $2.32 / 1 |
| Idler Shoulders | M4x5x20 | 5 | - | 92981A042 | $2.33 / 1 |
| NEMA 23 Stepper Motor | > 56mm Can Length | 1 | Spec Shaft with Pinion | [Stepper Online](https://www.omc-stepperonline.com/nema-23-stepper-motor/nema-23-bipolare-1-8deg-1-26nm-178-4oz-in-2-8a-2-5v-57x57x56mm-4-fili.html) | $14.83 / 1 |
| GT2 Pinion | 20T | 1 | Spec Shaft with NEMA 23 | [Amazon](https://www.amazon.com/Saiper-GT2-Teeth-6-35mm-Synchronous/dp/B07MGMBX3N/) (or) [RobotDigg](https://www.robotdigg.com/product/226/20-Tooth-2GT-Pulley-10pcs-per-lot) | $9.96 / 5 |
| GT2 Closed Loop Belt | 280T, 6mm Wide | 1 | - | [Amazon](https://www.amazon.com/280-2GT-6-Timing-Belt-Closed-Loop/dp/B014SLWP68/) (or) [RobotDigg](https://www.robotdigg.com/product/283/260mm-264mm-268mm-280mm-284mm-288mm-or-294mm-gt2-endless-belt) | $15.88 / 10 |
| GT3 Open Loop Belt | ~ 1m, 9mm Wide | 1 | - | [Amazon](https://www.amazon.com/Ochoos-Timing-Rubber-Backlash-5Meters/dp/B07MWD9D7V/) (or) [RobotDigg](https://www.robotdigg.com/product/597/3GT-9mm-or-6mm-wide-open-ended-belt) | $15.00 / 1 |
| 6806 Bearing | 30x42x7mm | 2 | x | 6806-2RS [VXB](https://www.vxb.com/6806-2RS-Bearing-30x42x7-Sealed-p/6806rs.htm) | $12.95 / 1 |
| M3 Inserts | Tapered, 3.8mm Tall | ~ 300 | - | 94180A331 | $12.92 / 100 |
| M4 Inserts | Tapered, 4.7mm Tall | 16 | - | 94180A351 | $14.96 / 100 |
| M5 Inserts | Tapered, 6.7mm Tall | 4 | - | 94180A361 | $12.00 / 50 |
| M3 Washers | 18-8 Standard Flat | 300 | - | 93475A210 | $1.62 / 100 |
| SHCS | M3x16 | 100 | Pinning T- to the wall, and pulley endmatter | 91292A115 | $5.87 / 100 |
| SHCS | M3x20 | 100 | Pinning T- to one another (?) | 91292A123 | $6.78 / 100 |
| SHCS | M3x25 | 100 | Pinning T- and X-es together | 91292A020 | $7.23 / 100 |
| SHCS | M3x45 | 8 | Pinning the 'squat' together | 91292A025 | $12.21 / 50 |
| FHCS | M3x10 | 30 | Pinning blisters to faces | 92125A130 | $5.81 / 100 |
| FHCS | M3x25 | (?) | - | 92125A138 | $11.50 / 100 |
| Steel Dowel Pins | 5x8mm | 10 | - | 91585A506 | $8.40 / 25 |
| Feet | M4 Stud, 15x15mm | - | - | 93115K881 | $1.78 / 1 |
| **Approximate Mechanical Total** | | | | | **$446.80** |
| Loadcell(s) | 10, 30, or 50kg | 1 | Choose Range for Sensitivity | [Amazon 50kg](https://www.amazon.com/Pressure-Force-S-type-Sensor-Cable/dp/B01F6IOW3G/) [Amazon 30kg](https://www.amazon.com/Pressure-Force-S-type-Sensor-Cable/dp/B01F6IOWDG/) [Amazon 10kg](https://www.amazon.com/Pressure-Force-S-type-Sensor-Cable/dp/B01F6IOW4K/) | $39.00 / 1|
| Loadcell Amplifier | HX711 | 1 | - | [Sparkfun 13879](https://www.sparkfun.com/products/13879) | $9.95 / 1 |
| Power Supply | 350W 24V LRS-350-24 | 1 | - | [Amazon](https://www.amazon.com/MEAN-WELL-LRS-350-24-350-4W-Switchable/dp/B013ETVO12/) | $32.25 / 1 |
| **Rough Control Total: Incomplete** | | | | | **81.20** |
| **All Up** | | | | | **$528.00** |

### PRINTED PARTS

The number of parts in the Fusion (CAD) model do not match the actual number of parts that are required for the machine. We are in the process of validating the numbers, it is better to follow this chart:

| Part | Spec | Count | Notes |  Picture | 
| --- | --- | ---: | --- | --- | 
| Blister Mirror | - | 3 | M3 inserts | <img src="images/blister_mirror.jpg"  width="50" >  |
| Blister | - | 3 | M3 inserts | <img src="images/blister.jpg"  width="50" >  |
| Edge Idler | - | 3 | - | <img src="images/edge_idler.jpg"  width="50" >  |
| Edge Idler Mirror | - | 3 | - | <img src="images/edge_idler_mirror.jpg"  width="50" >  |
| Pulley | - | 3 | - | <img src="images/pulley.jpg"  width="50" >  |
| Matter | - | 2 | - | <img src="images/matter.jpg"  width="50" >  |
| Matter Insert | - | 2 | - | <img src="images/matter_intert.jpg"  width="50" >  |
| Pincer Head | - | 10? | - | <img src="images/pincer_head.jpg"  width="50" >  |
| Pincer Shaft | - | 10? | - | <img src="images/pincer_shaft.jpg"  width="50" >  |
| Heart Rear Set | - | 3 | - | <img src="images/heart_rear_set.jpg"  width="50" >  |
| Heart Front Set| - | 3 | - | <img src="images/heart_front_set.jpg"  width="50" >  |
| Heart Drive Reduce | - | 3 | - | <img src="images/heart_drive_reduce.jpg"  width="50" >  |
| Heart Drive Pinon | - | 3 | - | <img src="images/heart_drive_pinion.jpg"  width="50" >  |
| Heart Drive Cap | - | 3 | - | <img src="images/heart_drive_cap.jpg"  width="50" >  |
| Squat | - | 2 | - | <img src="images/squat.jpg"  width="50" >  |
| Stray Idler | - | 1 | - | <img src="images/stray_idler_1.jpg"  width="50" >  |
| Stray Idler Mirror | - | 1 | - | <img src="images/stray_idler_2.jpg"  width="50" >  |

## Control and Interface

DEX runs a [squidworks](https://gitlab.cba.mit.edu/squidworks/squidworks) controller. The `dex` branches of [cuttlefish](https://gitlab.cba.mit.edu/squidworks/cuttlefish) and [ponyo](https://gitlab.cba.mit.edu/squidworks/ponyo) contain code that is known to work with the machine. Again, more documentation for these controllers is coming, for now - consult the repositories.

![c1](images/2019-10-17_dex-controller.png)
![c2](images/2019-10-17_dex-controller-zoom.png)

## Comparison to Instron 4411

To see how we do against a real instron, I tested identical samples on the DEX as well as on an Instron '4411' with a 5kN load cell. In the plot below (I'm using cuttlefish to plot the .csv that I saved from Bluehill, the Instron software), the leftmost plot is taken on the 4411, and the lazier slope belongs to the DEX.

While the samples fail around the same load, the difference in elongation is ~ 1.5mm wide: this is almost surely the machine's own deflection, stretch in the belts, etc.

![dex-compare](images/2019-10-17_data-compare.png)

This obviously warrants correction. One way to do this is to build a stiffer machine, however, we will be chasing up the cost and complexity if we do this. Rather, we should throw some more control at it. To start, we can circle back to our attempts at [subpixel tracking](https://gitlab.cba.mit.edu/calischs/subpixel_tracking), or attach a small linear stage directly to our fixturing elements. For this, I am imagining something like the [AMS5311](https://ams.com/as5311), which should do some 12 bits inside of a 2mm throw (for 0.4um resolution). Either can be added to existing systems, given network controllers / modular browser code. Since I want to integrate it elsewhere, it's likely that the camera option comes first.

## Testing Notes

The D683 ASTM Dogbones:

![dogbones](images/astm_d-638_bonesizes.jpg)

# Roadmap

*2019-10-17*

One complete DEX exists in the MIT CBA shop, and has been validated against an Instron 4411. Current efforts are twofold:

(1) Working with the [materiom](https://materiom.org/) project (whose mission: to provide open data on how to develop bio-inspired materials for a circular economy), and the [fablab at cic](https://cic.com/fab), we are replicating the current design outside of the CBA shop, and building documentation while we do.

(2) To improve the machine's test accuracy, we are integrating CV processing into the control architecture, to use [subpixel tracking](https://gitlab.cba.mit.edu/calischs/subpixel_tracking). Further controller improvements from the squidworks project will also bring rate control on-board.

## Notes
```
- Where to build logs go? This repo -> gitlab, mtm.cba, etc.
- Future integration of Temperature and Humidity Sensors
- Higher quality integration of load-cell amplifier
- Developing control for Cyclic Loading, Fatigue, etc.
- Developing direct upload / websocket-to-database tools
- Dec. 5: controllers -> CIC and London
- Cuttlefish: manipulation, understanding, computing on test datasets in the browser.
```

## TODO
```
- encapsulate branches for ponyo, cuttlefish.
- bs ?
- put rpi setup back on desk with dex, branch, close
- revisit with cameras, rate control
```
