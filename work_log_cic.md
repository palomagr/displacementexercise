Fusion model:
  the model needs cleanup and to make explicit which parts are reprensenting
  mechanical parts, which parts are to be 3 D printed
  and which parts are to be laser cut.
 
 3 D Printed:
 I sent 2 jobs to 3 D print with ABS material to the Stratasys
 machine. Each job was ~20 hours. The resolution is very good in the finished
 parts.
 ![image software](images/MVIMG_20191010_111954.jpg)
 
 Laser Cut: 
 I used Delrin 6mm thick with Epilog LEGEND EXT 120 watts. The settings were 20% speed
 100% power, 500 frequency. 4 passes each piece. The material gets heated
 and need to take turns in cutting each piece. Had to do several tests to understan
 the settings of the laser cutter. While cutting, many 'angels'
 appear (flames). Also, need to clean the lens
 of the laser cutter several times in between jobs.
 The fumes are very toxic. I will try to replace some parts with acrylic


<img src="images/IMG_20191017_152130.jpg"  width="300" >
<img src="images/15713404308557772465466104206724.jpg"  width="300" >
<img src="images/m1.jpg"  width="300" >
<img src="images/m2.jpg"  width="300" >
<img src="images/m3.jpg"  width="300" >
  